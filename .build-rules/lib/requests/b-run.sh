#!/bin/sh

if [ \! -d releases/cache/requests-1.2.0 ]; then
    mkdir -p releases/cache/
    pip install -q --no-install -d releases/cache requests==1.2.0
    cd releases/cache
    tar -xzf requests-1.2.0.tar.gz
fi
